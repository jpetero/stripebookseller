const express = require('express');

const keys = require('./config/keys');

const stripe = require('stripe')(keys.stripeSecretKey);

const bodyParser = require('body-parser');

const handlebars = require('express-handlebars');

const app = express();

// Middlewares
app.use(express.static(__dirname));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.engine('hbs', handlebars({ defaultLayout: 'main', extname: '.hbs' }));

app.set('view engine', 'hbs');

// Set the static folder
app.set('views', `${__dirname}/views`);

// Index route
app.get('/', (req, res) => {
  res.render('index', {
    stripePublishableKey: keys.stripePublishableKey
  });
});

app.post('/charge', (req, res) => {
  const amount = 2500;
  stripe.customers
    .create({
      email: req.body.stripeEmail,
      source: req.body.stripeToken
    })
    .then(customer =>
      stripe.charges.create({
        amount,
        description: 'Web Development Ebook',
        currency: 'usd',
        customer: customer.id
      })
    )
    .then(charge => res.redirect('/success'));
});

// Success route
app.get('/success', (req, res) => {
  res.render('success');
});

const port = process.env.PORT || 5000;

app.listen(port, () => {
  console.log(`Server running @ http://localhost:${port}`);
});
